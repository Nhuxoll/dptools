# -*- coding: utf-8 -*-
"""
Created on Wed Aug 8 10:41:55 2016

@author: nhuxoll
"""
import sys
sys.path.append('../src/dptools')
import numpy as np
import grids
import gridsio
import time
bohrangstrom = 0.529177249
angstrombohr = 1./bohrangstrom


def read_eigenvec(path=""):
    """ opens a eigenvec.out file and writes its content as np.array
   Args:
       path (filename or path+filename): path for the file to read
   Returns:
       np.array with eigenvectors
   """
    if path is "":
        path = "../../h2o/calc/eigenvec.out"
    eigenvec = np.ndarray(shape=(6, 6))
    buffer_list1 = []
    buffer_list2 = []
    eigenvec_f = open(path, "r")
    lines = eigenvec_f.readlines()
    eigenvec_f.close()
    for line in lines:
        k = line.split()
        if len(k) > 0:
            buffer_list1.append(k)
    #go through all non empty lines and search for a single digit
    #if a single digit is found, the following 6 lines contain eigenvalues
    #s,p1,p2,p3    s1    s1
    for i, list in enumerate(buffer_list1):
            if list[1].isdigit():
                buffer = [float(buffer_list1[i+1][1]),
                          float(buffer_list1[i+2][1]),
                          float(buffer_list1[i+3][1]),
                          float(buffer_list1[i+4][1]),
                          float(buffer_list1[i+5][1]),
                          float(buffer_list1[i+6][1])]
                buffer_list2.append(buffer)
    eigenvec = np.asarray(buffer_list2)
    return eigenvec


def testinput(fname='H2O_testinput'):
    """first version of a function to read an input file for Wavpy.Molecule
    Args:
        fname (filename or path+filename): path for the file to read
    Returns:
        list of parameters
        [morigin, resolution, mrange, geometry_source, anumbers, atomdict]
    """
    print('start reading file...')
    f = open(fname, 'r')
    k = f.readlines()
    morigin = [float(i) for i in k[0].split()[1:]]
    resolution = float(k[1].split()[1])
    mrange = [int(i) for i in k[2].split()[1:]]
    geometry_source = k[3].split()[1]
    anumbers = int(k[7].split()[1])
    total_atoms = int(k[7].split()[2])
    density = [int(i) for i in k[4].split()[1:]]
    wavefunctions = [int(i) for i in k[5].split()[1:]]
    eigenvec = k[6].split()[1]
    atom_dict = dict()
    line = 8
    leng = len(k)
    i = 1
    while line < leng:
        i = int(k[line].split()[1])
        if i is 1:
            atom_dict[k[line].split()[0]] = {'l': {0: k[line+1].split()[0]},
                                            'resolution': k[line+2].split()[0],
                                            'range': k[line+2].split()[1:]}
            line += 3
        elif i is 2:
            atom_dict[k[line].split()[0]] = {'l': {0: k[line+1].split()[0],
                                                   1: k[line+2].split()[0]},
                                            'resolution': k[line+3].split()[0],
                                            'range': k[line+3].split()[1:]}
            line += 4
        elif i > 2 or not i:
            print('break')
            break
    f.close()
    print('finished reading file')
    print("start calculating...")
    k = Molecule(orbitals=None,
                 atom_number=anumbers,
                 geometry_source=geometry_source,
                 eigenvector=eigenvec,
                 origin=morigin,
                 atom_dict=atom_dict,
                 mresolution=resolution,
                 mrange=mrange,
                 density=density,
                 total_atoms=total_atoms,
                 wavefunctions=wavefunctions).create_molecule()
    print('done!')


class Molecule:
    """
    """
    def __init__(self,
                 orbitals=None,
                 atom_number=3,
                 geometry_source='../../h2o/calc/geo.xyz',
                 eigenvector=None,
                 origin=[0, 0, 0],
                 atom_dict=None,
                 total_atoms=3,
                 mresolution=0.2,
                 mrange=[-200, 200],
                 density=[0, 0],
                 wavefunctions=[1, 4]):
        self.orbitals = []
        self.atom_number = atom_number
        self.geometry_source = geometry_source
        self.geometry = None
        self.test_dict = {}
        if eigenvector is None:
            eigenvector = "../../h2o/calc/eigenvec.out"
        self.eigenvec_source = eigenvector
        self.eigenvec = read_eigenvec(self.eigenvec_source)
        self.molecule_origin = origin
        self.intersection = []
        self.total_atoms = total_atoms
        self.orbital_resolution = 0.1
        self.mrange = mrange
        ranges = [[self.mrange[0], self.mrange[1]],
                 [self.mrange[0], self.mrange[1]],
                 [self.mrange[0], self.mrange[1]]]
        self.molecule_resolution = mresolution
        self.mbasis = [[self.molecule_resolution, 0.0, 0.0],
                       [0.0, self.molecule_resolution, 0.0],
                       [0.0, 0.0, self.molecule_resolution]]

        self.molecule_grid = grids.Grid(origin=self.molecule_origin,
                                        basis=self.mbasis,
                                        ranges=ranges
                                        )
        self.molecule_data = np.zeros(self.molecule_grid.shape)
        self.molecule_grid_data = grids.GridData(self.molecule_grid,
                                                 self.molecule_data)
        self.atom_dict = atom_dict
        if atom_dict is None:
            self.atom_dict = {'H': {'l': {0: '../../h2o/data/h_1s.dat'},
                                    'resolution': 0.1},
                              'O': {'l': {0: '../../h2o/data/o_2s.dat',
                                          1: '../../h2o/data/o_2p.dat'},
                                    'resolution': 0.1}}
        self.atoms = {}
        self.molecule_orbitals = []
        self.period_sys = {'H': 1, 'He': 2, 'Li': 3, 'Be': 4,
                           'B': 5, 'C': 6, 'N': 7, 'O': 8,
                           'F': 9, 'Ne': 10, 'Na': 11, 'Mg': 12,
                           'Al': 13, 'Si': 14, 'P': 15, 'S': 16,
                           'Cl': 17, 'Ar': 18, 'K': 19, 'Ca': 20,
                           }

    def timed_molecule(self):
        """function to time the creation of a molecule
        """
        startTime = time.clock()
        self.create_molecule()
        runl = float(time.clock() - startTime)
        print("Time elapsed: %f s" % runl)

    def create_orbital(self, l=1, source=None):
        """create all needed orbitals
        Args:
            l(int): azimutal quantum number
            source(str): path to orbital data
        Returns:
            list containing all orbitals for the corresponding l
        """
        res = self.orbital_resolution
        if source is None:
            source = '../../h2o/data/o_2p.dat'
        orbital = Orbital(resolution=res,
                          source=source,
                          l=l,
                          cutoff=None)
        self.orbitals.append(orbital)
        return orbital

    def create_atom(self):
        """fills self.atoms
        writes dict with following scheme:
            {Atom name:{'l':{azimutalquantum number: source of data},
                       'resolution': float}}
        """
        self.atoms = {}
        for key, item in self.atom_dict.items():
            self.atoms[key] = []
            for orbit, source in item['l'].items():
                self.atoms[key].append(self.create_orbital(l=orbit,
                                                           source=source))

    def get_geometry(self):
        """ fills list with geometry
        writes geometry data into self.geometry:
            self.geometry = [[number of atoms], [],[atom name, x, y, z]]
        """
        self.geometry = []
        f = open(self.geometry_source, 'r')
        lines = f.readlines()
        for line in lines:
            self.geometry.append(line.split())

    def create_molecule(self):
        """create molecule data with given atoms and geometry
        """
        print('read geometry')
        if self.geometry is None:
            self.get_geometry()
        print('create atoms')
        self.create_atom()
        print('build basic molecule')
        self.build_molecule()
        print('write cube file')
        print('please be patient')
        self.test_mol()

    def build_molecule(self):
        for atom in self.geometry[2:]:
            for orbitals in self.atoms[atom[0]]:

                for orbit in orbitals.orbitals:
                    # app. 0.9s per iteration step (per Orbital)
                    grid = self.molecule_grid_data.grid
                    molgrid = self.molecule_grid_data
                    orbit[1].grid.set_origin([atom[1], atom[2], atom[3]])
                    intersect = grid.get_intersection_grid(orbit[1].grid)
                    intersect_cart = intersect.\
                        get_gridpoints('CARTESIAN_COORD')
                    intersect_view = molgrid.get_subgrid_dataview(intersect)
                    intersect_values = orbit[1].\
                        get_interpolated_value(intersect_cart,
                                               'CARTESIAN_COORD')
                    intersect_values = intersect_values.\
                        reshape(intersect_view.shape)
                    self.intersection.\
                        append([intersect_view, intersect_values])

    def test_mol(self, maxi=[1, 4]):
        """dummy function for eigenvectors
        """
        for i, intersect in enumerate(self.intersection):
            while i in range(maxi[0], maxi[1]+1):
                for k in self.eigenvec[i]:
                    intersect[0] += intersect[1]*k
                i += 1
        #gridsio.cube('gridstest.cube', self.molecule_grid_data,
        #             header='dptools cube file')
        self.write_out()

    def write_out(self, fname='test'):
        fname += '.cube'
        f = open(fname, 'w')
        f.write('Cube File \n')
        f.write('OUTER LOOP: X,\t INNER LOOP: Y,\t INNER LOOP: Z \n')
        f.write(str(self.total_atoms) + '\t' + str(self.molecule_origin[0])
                + '\t' + str(self.molecule_origin[1])
                + '\t' + str(self.molecule_origin[2])+'\n')
        f.write(str(self.mrange[1]*2) + '\t' + str(self.mbasis[0][0])
                + '\t' + str(self.mbasis[0][1])
                + '\t' + str(self.mbasis[0][2]) + '\n')
        f.write(str(self.mrange[1]*2) + '\t' + str(self.mbasis[1][0])
                + '\t' + str(self.mbasis[1][1])
                + '\t' + str(self.mbasis[1][2]) + '\n')
        f.write(str(self.mrange[1]*2) + '\t' + str(self.mbasis[2][0])
                + '\t' + str(self.mbasis[2][1])
                + '\t' + str(self.mbasis[2][2]) + '\n')
        for atom in self.geometry[2:]:
            f.write(str(self.period_sys[atom[0]])+'\t' + '0.000000\t'
                    + str(atom[1])+'\t'
                    + str(atom[2])+'\t'
                    + str(atom[3])+'\n')
        self.molecule_grid_data.data.flatten(order='C').\
            tofile(fname, sep=' ')
        f.close()


class Orbital:
    """
    """
    def __init__(self,
                 resolution=0.1,
                 source='../../h2o/data/o_2p.dat',
                 l=1,
                 cutoff=None):
        self.source = source
        self.azim = l
        self.spherical = []
        self.orbital_grid = None
        self.cartesian_orb_grid = None
        self.orbital_radii = None
        self.orbitals = []
        self.resolution = resolution
        self.basis = [[self.resolution, 0.0, 0.0],
                      [0.0, self.resolution, 0.0],
                      [0.0, 0.0, self.resolution]]
        self.origin = [0.0, 0.0, 0.0]
        self.radial_wavefunc = np.loadtxt(source)
        self.orbital_values = []
        self.make_orbit()

    def make_orbit(self):
        """creates/calculates grid, spherical harmonical and columetric data
        """
        self.create_orbital_grid()
        self.spherical_harmonical()
        self.get_interpolated_orbitals()

    def create_orbital_grid(self):
        """create orbital grid with given origin and basis
        """
        self.orbital_grid = grids.Grid(origin=self.origin,
                                       basis=self.basis,
                                       ranges=[[-60., 60.],
                                               [-60., 60.],
                                               [-60., 60.]])
        self.cartesian_orb_grid = self.orbital_grid.\
            get_gridpoints('CARTESIAN_COORD')
        self.orbital_radii = np.sqrt(np.sum(self.cartesian_orb_grid**2,
                                            axis=1))

    def spherical_harmonical(self):
        """Calculates the spherical harmonical of the wavefunction
        fills self.spherical for the orbital
        """
        if self.azim is 0:
            self.spherical.append(1/np.sqrt(4*np.pi))
        if self.azim is 1:
            fact = 0.4886025119029198
            self.spherical.append(np.nan_to_num(fact
                                  * np.divide(self.cartesian_orb_grid[:, 1],
                                              self.orbital_radii)))
            self.spherical.append(np.nan_to_num(fact
                                  * np.divide(self.cartesian_orb_grid[:, 2],
                                              self.orbital_radii)))
            self.spherical.append(np.nan_to_num(fact
                                  * np.divide(self.cartesian_orb_grid[:, 0],
                                              self.orbital_radii)))

    def get_interpolated_orbitals(self):
        """Interpolate the values of the wavefunction for the given grid
        Returns:
            list with np.arrays for each magnetic quantum number
        """
        gridData = np.zeros(self.orbital_radii.shape)
        xp = self.radial_wavefunc[:, 0]
        fp = self.radial_wavefunc[:, 1]
        gridData = np.interp(self.orbital_radii, xp, fp)
        if self.azim is 0:
            orbit_00 = (gridData*self.spherical[0]).reshape((120,
                                                            120,
                                                            120))
            self.orbitals.append([0,
                                  grids.GridData(self.orbital_grid, orbit_00)])
            self.orbital_values.append(orbit_00)
        elif self.azim is 1:
            orbit_10 = (gridData*self.spherical[0]).reshape((120,
                                                             120,
                                                             120))
            orbit_11 = (gridData*self.spherical[1]).reshape((120,
                                                             120,
                                                             120))
            orbit_12 = (gridData*self.spherical[2]).reshape((120,
                                                             120,
                                                             120))
            self.orbital_values.append(orbit_10)
            self.orbital_values.append(orbit_11)
            self.orbital_values.append(orbit_12)
            self.orbitals.append([0,
                                 grids.GridData(self.orbital_grid, orbit_10)])
            self.orbitals.append([1,
                                 grids.GridData(self.orbital_grid, orbit_11)])
            self.orbitals.append([2,
                                 grids.GridData(self.orbital_grid, orbit_12)])
        return self.orbitals

    def get_accuracy(self, m=0):
        acc = np.sum((self.orbital_values[m])**2)*self.resolution**3
        return acc

    def set_origin(self, origin=[0, 0, 0]):
        """sets origin for the orbital
        calls orbital.grid.set_origin
        """
        for orbital in self.orbitals:
            orbital.grid.set_origin(origin)

    def get_oribtal_cube(self, fname='test'):
        """write gaussian cube file for each orbital
        """
        if self.azim is 0:
            gridsio.cube('../../'+fname, self.orbitals[0])
        if self.azim is 1:
            self.orbitals[0].data.flatten(order='C').tofile('../../p0_'
                                                            + fname + '.cube',
                                                            sep=' ')
            self.orbitals[0].data.flatten(order='C').tofile('../../p1_'
                                                            + fname + '.cube',
                                                            sep=' ')
            self.orbitals[2].data.flatten(order='C').tofile('../../p2_'
                                                            + fname + '.cube',
                                                            sep=' ')
